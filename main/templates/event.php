<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Bootstrap Boilerplate</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap -->

    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../css/flat-ui.min.css">
    <link rel="stylesheet" href="../../../css/event.css">

    <!-- Font-Awesome -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="//ajax.aspnetcdn.com/ajax/jQuery/jquery-1.11.1.min.js"><\/script>')</script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

    <style>
        ul li{
            font-size: 17px;
            font-family: Lato, Helvetica, Arial, sans-serif;
            padding: 5px 2px;
        }

        .dark-box{
            background-color: #16A085;
            color: #ffffff;
            border-color: #1ABC9C;
        }

        .event-title{
            color: #16A085;
        }
    </style>
</head>
<body>
<?php

    $id = $this->data['id'];
    $result = $this->data['result'];
    $event = $result->event;

//    $flightRes = [];
    $flightRes = json_decode($this->data['flightRes']);

//pointers
    $fQuery = ($flightRes->Query);
    $fPlaces = ($flightRes->Places);
    $fLegs = ($flightRes->Legs);
    $fCarriers = ($flightRes->Carriers);
    $fItineraries = ($flightRes->Itineraries);

$fOutboundDate = $fQuery->OutboundDate;
$fDestinationPlace = $fQuery->DestinationPlace;
$fOriginPlace =  $fQuery->OriginPlace;


$AirportName = "";
$AirportCode = "";


//$fParentAPIdto = $fPlaces->ParentApiDto;




//    print_r($flightRes);
echo count($flightRes);




//print_r($flightRes->SessionKey);

    $venue = $event->venue->name;
    $street = $event->venue->location->street;
    $city = $event->venue->location->city;
    $country = $event->venue->location->country;
    $postalcode = $event->venue->location->postalcode;

    $website = $event->venue->website;
    $phonenumber = $event->venue->phonenumber;

    $image = $event->image[3]->{"#text"};
    $startDate = $event->startDate;
    $attendance = $event->attendance;
    $tickets = $event->tickets;
    $cancelled = $event->cancelled;
    $tags = $event->tags;


//flights


?>

<div class="row demo-tiles">
    <div class="col-lg-3 col-lg-offset-1">
        <div class="tile">
            <h3 class="tile-title">Event details</h3>

        </div>
    </div>

    <?php


    //        $isError= false;
    //
    //        if (empty($result[error]))
    //        {
    //            $isError = true;
    //        }
    //
    //        echo var_export($isError);


    ?>


    <br>
    <div class="row">
        <div class="col-xs-10 col-xs-offset-1">
            <nav class="navbar navbar-inverse navbar-embossed" role="navigation">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-01">
                        <span class="sr-only">Toggle navigation</span>
                    </button>
                    <a class="navbar-brand" href="../../../">Eventual</a>
                </div>
                <div class="collapse navbar-collapse" id="navbar-collapse-01">
                    <ul class="nav navbar-nav navbar-right">

                    </ul>
                </div><!-- /.navbar-collapse -->
            </nav>
        </div>
    </div>

    <div class="row demo-tiles">
        <div class="col-lg-3 col-lg-offset-1">
            <h3 class="tile-title event-title"><?php echo $event->title ?></h3>
            <br>
            <div class="bordered well dark-box">
                <div class="row">
                    <div class="col-xs-10 col-xs-offset-1">
                        <!-- ~~~~~~~~~~~~~~~ -->
                            <div id="img-container">
                                <img class="img-rounded" src="<?php echo $event->image[3]->{"#text"} ?> "/>
                            </div>
                            <div id="detail-container">
                                <ul class ="list-unstyled">

                                    <?php


                                    if (!empty($startDate)) {
                                        echo '<li class="clearfix"> <i class="fa fa-calendar fa-fw"></i>' . $startDate . "</li>";
                                    }

                                    if (!empty($venue)) {
                                        echo '<li class="clearfix"> <i class="fa fa-institution fa-fw"></i>' . $venue . "</li>";
                                    }

                                    if (!empty($city)) {
                                        echo '<li class="clearfix"> <i class="fa fa-map-marker fa-fw"></i>';
                                    if (!empty($street)) {
                                        echo $street;
                                        if (!empty($postalcode)) {
                                            echo ", ". $postalcode . "<br/>";
                                        }
                                    }

                                        echo $city;
                                        if (!empty($country)) {
                                            echo ", " . $country;
                                        }
                                        echo "</li>";
                                    }

                                    echo "<br/>";

                              if ((!empty($website)) || (!empty($phonenumber))) {
                                  echo '<li class="clearfix">';
                                  if (!empty($website)) {
                                      echo '<i class="fa fa-link fa-fw"></i> <a href= " ' . $website . '"> Link</a>&nbsp;&nbsp;&nbsp;';
                                  }
                                  if (!empty($phonenumber)) {
                                      echo '<i class="fa fa-phone fa-fw"></i>' . $phonenumber;
                                  }
                                  echo "</li>";
                              }

                            if (!empty($attendance)) {
                                echo '<li class="clearfix"> <i class="fa fa-user fa-fw"></i> <em>' . $attendance . " people attending</em></li>";
                            }

                            if (!($tickets)) { //buggy
                                echo '<li class="clearfix"> <i class="fa fa-ticket fa-fw"></i> <em>' . $tickets . " tickets available</em></li>";
                            }
                                    ?>


                                </ul>
                            </div>
                        <!-- ~~~~~~~~~~~~~~~~~ -->
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-7">
            <div class="">
                <?php
                foreach ($fPlaces as $p) {
                    if (isset($p->ParentId)) {

                        if ($fDestinationPlace == $p->ParentId) {
                            $AirportName = $p->Name;
                            $AirportCode = $p->Code;
                        }
                    }

                }

                echo '<h3 class="tile-title">Flight bookings <span class="badge badge-secondary">Edinburgh (EDI) to  ' . $AirportName ." (". $AirportCode . ")".'</span></h3>';
                ?>
                <br>
                <ul class="list-group">
                    <?php

//                       echo " AIRPORT STUFF GOES HERE";



                    $flights[]= Array();
                    $i = 0;
                    foreach ($fLegs as $p) {
                        $flights[$i]['Id'] = $p->Id;
                        $flights[$i]['Departure'] = substr($p->Departure, 11);
                        $flights[$i]['Arrival'] = substr($p->Arrival, 11);
                        $flights[$i]['Duration'] = date('H:i', mktime(0, $p->Duration));
                        $flights[$i]['Stops'] = count($p->SegmentIds);

                        foreach($fItineraries as $q){
                            $d = $q->OutboundLegId;
                            $price = $q->PricingOptions['0']->Price;
                            $link = $q->PricingOptions['0']->DeeplinkUrl;

                            if ($d == $flights[$i]['Id'])
                            {
                                $flights[$i]['PriceOpt'] = $price;
                                $flights[$i]['link'] = $link;
//                                echo 'hi';
                            }
                        }

                        $i++;
                    }



//                    print_r($flights);




//                    $j = 0;
//                    foreach ($fPlaces as $p) {
//                        //                        if ($fDestinationPlace == $p->ParentId)
//
//                        $flights[$j]['AirportName'] = $p->Name;
//                        $j++;
//                    }


//                    echo "<pre>". print_r($flights) . "</pre>";
                    
                    ?>

                    	<thead>
                    		<tr>
                    			<th></th>
                    		</tr>
                    	</thead>
                    	<tbody>
                    		<tr>
                    			<td></td>
                    		</tr>
                    	</tbody>
                    </table>



                   <table class="table table-striped table-hover">
                       <thead>
                         <th>Departs</th>
                         <th>Arrives</th>
                         <th>Duration</th>
                         <th>Stops</th>
                         <th>Price</th>
                       <th>Book</th>

                       </thead>
                    <tbody>
                        <?php
                            foreach ($flightRes as $p) {

                            }
                        foreach ($flights as $key => $p) {
                            echo "<tr>";
                            echo "<td>" . $p['Departure'] . "</td>";
                            echo "<td>" . $p['Arrival'] . "</td>";
                            echo "<td>" . $p['Duration'] . "</td>";
                            echo  "<td>" .$p['Stops'] . "</td>";
                            echo "<td>" . $p['PriceOpt'] . "</td>";
//                            echo '<td> <input href="'. $p['link'].'" class="btn btn-secondary">Book</a>';
                            echo '<td><a class="btn btn-secondary" type="button" href="'. $p['link'].'">
                                Book </a></td>';


                            echo "</tr>";

                        }
                        foreach ($flightRes as $p) {
                        }

        //                   echo "</table>";
        //                            echo $AirportName = $p->Name;
        //                            echo $AirportCode = $p->Code;
        //                        }

                        ?>
                 <tbody>
                 </table>





                    ?>
                </ul>
            </div>
        </div>
    </div>

</div>


</body>
</html>