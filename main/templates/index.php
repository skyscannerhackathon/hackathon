<?php
set_time_limit(0);

      function get_cheapest_flight_price($destination_place, $out, $in = "" ,$apikey = "ilw23772659141790428290476649156"){
        $origin_place="EDI";
        $url = "http://partners.api.skyscanner.net/apiservices/browsegrid/v1.0/". "GB/GBP/en-GB/" . $origin_place ."/" . $destination_place . "-Latlong/" . $out;
        

        if(empty($in) == true){
            $url = $url . "/" . $in;
        }
        $url = $url . "?apiKey=" . $apikey;

        $context = stream_context_create(array('http' => array('header' => 'Accept: application/json')));
        $json = json_decode(file_get_contents($url, false, $context));
        return (isset($json->Dates[1][0]->MinPrice) ? $json->Dates[1][0]->MinPrice : "");

    }

    function autosuggest($query){
        $url = "http://partners.api.skyscanner.net/apiservices/hotels/autosuggest/v2/UK/GBP/en-GB/" . $query . "?apikey=ilw23772659141790428290476649156";
        $context = stream_context_create(array('http' => array('header' => 'Accept: application/json')));
        $json = json_decode(file_get_contents($url, false, $context));
        
        $ids = array();
        foreach($json->results as $id){
           
            $a = $id->individual_id;
            array_push($ids,$a);
        }

        //$id=$json->results[0]->individual_id;
        //print_r($id);
        $slice = array_slice($ids,0,2);
        return $slice;
    }

    function get_hotel_pricing($checkindate,$checkoutdate,$guests,$rooms,$location,$pagesize,$imagecount){

       

        $apikey="ilw23772659141790428290476649156";
        $entities = autosuggest($location);
        $LP=9999999999999;
        foreach ($entities as $entityid) {
            
            $url = "http://partners.api.skyscanner.net/apiservices/hotels/liveprices/v2/UK/GBP/en-GB/"  . $entityid ."/". $checkindate . "/" . $checkoutdate ."/" . $guests . "/" .  $rooms . "?apiKey=" . $apikey . "&pagesize=" . $pagesize;
            
            $context = stream_context_create(array('http' => array('header' => 'Accept: application/json')));
            $json = json_decode(file_get_contents($url, false, $context));
            $a = $json->hotels_prices; 
            foreach($a as $prices){
                    $p = isset($prices->agent_prices[0]->price_total) ? $prices->agent_prices[0]->price_total : 204.99;
                  
                    if($p<$LP){
                            $LP = $p;
                    }        
            }
        }
        return $LP;
    }

            foreach (array_slice($this->data->allEvents, 0, 20) as $key => $event) {

              $geo = $event->venue->location->{'geo:point'};
              $lat = $geo->{'geo:lat'} ? : "10.0";
              $lng = $geo->{'geo:long'} ? : "10.0";

              if (strcmp($lat, "") != 0) {
                $date = strtotime($event->startDate);
                $date = date("Y-m-d", $date);

                $price = get_cheapest_flight_price($lat . "," . $lng, $date);
                if (!(strcmp($price, "") == 0)) {
                  $event->price = $price;
                }
              }
            }
?>

<html class="no-js" lang="">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Bootstrap Boilerplate</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap -->

    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
      <link rel="stylesheet" href="../../../css/flat-ui.min.css">
      <link rel="stylesheet" href="../../../css/demo.css">

    <!-- Font-Awesome -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="//ajax.aspnetcdn.com/ajax/jQuery/jquery-1.11.1.min.js"><\/script>')</script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script src="../../../js/flat-ui.min.js"></script>
    <script src="../../../js/application.js"></script>
  </head>
  <body>

  <?php

            function prettyPrint($a) {
              echo '<pre>'.print_r($a,1).'</pre>';
            }

            //$events = $this->data->allEvents;
            //prettyPrint($events);


  ?>

  <style type="text/css">
    div.eventImageDiv
    {
        width: 100px;
        height: 100px;
        overflow: hidden;
        margin: 0px;
        position: relative;
        border-radius: 8px;
    }
    img.eventImage  {
        position:absolute;
        left: -100%;
        right: -100%;
        top: -100%;
        bottom: -100%;
        margin: auto;
        min-height: 100%;
        min-width: 100%;
    }
    .left {
      text-align: left !important;
    }
    .bordered {
      position: relative;
      padding: 14px;
      margin-bottom: 20px;
      text-align: center;
      /* background-color: #eff0f2; */
      border-radius: 6px;
      border-width: 1px;
      border-style: solid;
      border: 1px solid #ddd;
    }
  </style>

  <script type="text/javascript">

  var sortType = "name";
  var sortValue = "asc";

    $( document ).ready(function() {

      $("#searchButton").click(function(event) {
        var keyword = $('#keyword').val();
        var location = $('#location').val();

        var url =  "/Skyscanner/main/keywords/"+keyword+"/locations/";
        if(location === '')
          url= url+"all";
        else
        url=url+location;

       window.location.href=url;

      });


      $("#sortAsc").click(function(event) {
        $("#sortAsc").addClass("active");//.attr("class", "active");
        $("#sortDesc").removeClass("active");//.attr("class", "");
        sortValue = "asc";
        sortBy(sortType, sortValue);
      });
      $("#sortDesc").click(function(event) {
        $("#sortDesc").addClass("active");//.attr("class", "active");
        $("#sortAsc").removeClass("active");//.attr("class", "");
         sortValue = "desc";
         sortBy(sortType, sortValue);
      });
      $("#sortName").click(function(event) {
        $("#sortName").addClass("active");//.attr("class", "active");
        $("#sortPrice").removeClass("active");//.attr("class", "");
        sortType = "name";
        sortBy(sortType, sortValue);
      });
      $("#sortPrice").click(function(event) {
        $("#sortPrice").addClass("active");//.attr("class", "active");
        $("#sortName").removeClass("active");//.attr("class", "");
        sortType = "price";
        sortBy(sortType, sortValue);
      });

      //sortBy(sortType, sortValue);

      function sortBy(nameOrPrice, ascOrDesc) {
        var isName = nameOrPrice == "name";
        var isAsc = ascOrDesc == "asc";

        var dataID = isName ? "name" : "price";

        var s = $('#eventList li').sort(function (a, b) {
            var contentA =( $(a).attr('data-'+dataID));
            var contentB =( $(b).attr('data-'+dataID));
            if (isAsc)
              return (contentA < contentB) ? -1 : (contentA > contentB) ? 1 : 0;
            return (contentA > contentB) ? -1 : (contentA < contentB) ? 1 : 0;
        });

        s.each(function(){
          $(this).html("<a href='/Skyscanner/main/event/" + $(this).attr("data-event") + "' style='color: #34495e;'>" + $(this).html() + "</a>");
        });

        $('#eventList').html(s);
      }

    });
  </script>

  <br>
    <div class="row">
      <div class="col-xs-10 col-xs-offset-1">
        <nav class="navbar navbar-inverse navbar-embossed" role="navigation">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-01">
              <span class="sr-only">Toggle navigation</span>
            </button>
            <a class="navbar-brand" href="../../../" style="padding-top: 6px;padding-bottom: 0px;padding-left: 20px; padding-right: 0px;">
            <img src="../../../img/logo-white.png" style="height:40px;">
            Eventual</a>
          </div>
          <div class="collapse navbar-collapse" id="navbar-collapse-01">
            <ul class="nav navbar-nav navbar-right">
              <li>
                <div class="tagsinput-primary navbar-right" style="style=width: 300px;padding-top: 5px; margin-bottom: 2px; padding-right: 5px;">
                <?php
                  $keywords = $this->data->keywords;
                  $keywordsString = implode(", ", $keywords);
                ?>
                  <input id="keyword" name="tagsinput" class="tagsinput" data-role="tagsinput" value=<?php echo "'" . $keywordsString . "'"; ?> style="display: none;">
                </div>
              </li>
              <li>
                <div class="tagsinput-primary navbar-right" style="style=width: 300px;padding-top: 5px; margin-bottom: 2px; padding-right: 5px;">
                <?php
                  $locations = $this->data->locations;
                  $locationsString = implode(", ", $locations);
                ?>
                  <input id="location" name="tagsinput" class="tagsinput" data-role="tagsinput" value=<?php echo "'" . $locationsString . "'"; ?> style="display: none;">
                </div>
              </li>
              <li>
                <a id="searchButton" href="javascript:;" class="btn btn-primary" style="border-top-width: 10px;padding-top: 5px;padding-bottom: 5px; margin-top: 10px;padding-right: 15px; padding-left: 15px; border-right-width: 5px; margin-right: 10px; margin-left: 10px;">
                Search
                </a>
              </li>
             </ul>
          </div><!-- /.navbar-collapse -->
        </nav>
      </div>
    </div>

    <div class="row demo-tiles">
      <div class="col-lg-3 col-lg-offset-1">
        <h3 class="tile-title">Filter</h3>
        <br>
        <div class="bordered">
          Sort by
          <br>
          <ul class="nav nav-pills" role="tablist">
            <li role="presentation" class="active" style="width:45%; padding-left:20px;" id="sortName"><a href="javascript:;">Name</a></li>
            <li role="presentation" style="width:45%; padding-left:20px;" id="sortPrice"><a href="javascript:;">Price</a></li>
          </ul>
          <br>
          <ul class="nav nav-pills" role="tablist">
            <li role="presentation" class="active" style="width:45%; padding-left:20px;" id="sortAsc"><a href="javascript:;">Ascending</a></li>
            <li role="presentation" style="width:45%; padding-left:20px;" id="sortDesc"><a href="javascript:;">Descending</a></li>
          </ul>
        </div>
      </div>

      <div class="col-lg-7">
        <div class="">
          <h3 class="tile-title">Search Results</h3>
          <br>
          <ul class="list-group eventList" id="eventList">
            <?php

            $events = $this->data->allEvents;
            //prettyPrint($events);

            $addedEvents = 0;
            foreach ($events as $key => $event) {
              if ($addedEvents > 40) {
                return;
              }

              if (!isset($event->price)) {
                break;
              }

                            $price = $event->price;


              $location = $event->venue->location->city . ", " . $event->venue->location->country;

              $sdate = strtotime($event->startDate) - (60 * 60 * 24);
              $sdate = date("Y-m-d", $sdate);
              $edate = strtotime($event->startDate) + (60 * 60 * 24);
              $edate = date("Y-m-d", $edate);
              $hotelPrice = get_hotel_pricing($sdate,$edate,1,1,"london",3,2);

              $addedEvents++;

              echo "<a href='/Skyscanner/main/event/" . $event->id . "' style='color: #34495e;'>";
                echo "<li class='list-group-item left' data-event='" . $event->id . "' data-name='" . $event->title . "' data-price='" . $price . "'>";

                  echo "<div class='row'>";

                    echo "<div class='col-xs-2'>";
                      echo "<div class='eventImageDiv'>";
                        echo "<img class='eventImage' src='" . $event->image[2]->{'#text'} . "' alt=''>";
                      echo "</div>";
                    echo "</div>";

                    echo "<div class='col-xs-5'>";
                      echo $event->title;
                      if (is_array($event->artists->artist)) {
                      echo "<br>";
                      echo "<p class='palette-paragraph'>" . implode(", ", $event->artists->artist) . "</p>";
                      }
                    echo "</div>";

                    echo "<div class='col-xs-5 text-right'>";
                        echo $location;
                        echo "<br>";
                        echo "<div style='padding-top:10px;'>Flights from £" . sprintf('%0.2f', $price);
                        echo "</div>";
                        echo "<div style='padding-top:10px;'>Hotels from £" . sprintf('%0.2f', $hotelPrice);
                        echo "</div>";
                    echo "</div>";

                  echo "</div>";

                echo "</li>";
              echo "</a>";
            }

            ?>
          </ul>
        </div>
      </div>
    </div>

<?php

?>

  </body>
</html>