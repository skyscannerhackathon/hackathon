<?php
require 'sky.php';
require 'Slim/Slim.php';
\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();



$app->get('/', function() use ($app) {
    $app->redirect("home.php");

});

$app->get('/hello/:name', function ($name) {
    echo "Hello " . $name;
});

$app->get('/event/:id', function ($id) use ($app) {
    $result = [];
//    $id="10229304";
    $result = json_decode(callAPI("http://ws.audioscrobbler.com/2.0/?method=event.getinfo&event=" . $id . "&api_key=685d1b03f751e03d38e3270b195570c1&format=json"));

    $app->view()->setData(array('id' => $id));
    $app->view()->setData(array('result' => $result));
    $event = $result->event;

    $geo = $event->venue->location->{'geo:point'};
    $lat = $geo->{'geo:lat'};
    $lng = $geo->{'geo:long'};
    if (strcmp($lat, "") == 0) {
        break; // sometimes there are no coordinates
    }
    $dest = $lat.','.$lng;

    $date = strtotime($event->startDate);
    $date = $date - (60*60*24);
    $date = date("Y-m-d", $date);

//    echo $dest;
//    echo $date;
    $sky = new Sky();
    $session = $sky->create_session('EDI',$dest,$date,'',1,0,0);

    $flightRes = $sky->get_live_pricing($session,"ilw23772659141790428290476649156");
    $app->view()->setData(array('flightRes' => $flightRes));

    $app->render('event.php');
});

$app->get("/keywords/:keywords/locations/:locations", function ($keywords, $locations) use ($app) {
  $keywords = explode(',', $keywords);
  $locations = explode(',', $locations);

  $allEvents = [];

  $genres = genres($keywords);
  $artists = artists($keywords);

  $allEvents = addAllArtistAndLocationEvents($artists, $locations, $genres);


  //print_r(array_map("mapID", $allEvents));



  $app->view()->setData(array('allEvents' => $allEvents));
  $app->view()->setData(array('keywords' => $keywords));
  $app->view()->setData(array('locations' => $locations));
  $app->render('index.php');


});

$app->run();



function removeDuplicates($events) {

}

function callAPI($url)
{
    $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HEADER, 0);
     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
     curl_setopt($ch, CURLOPT_TIMEOUT, 10);
     $output = curl_exec($ch);
     curl_close($ch);

    return $output;
}

function addAllArtistAndLocationEvents($artists, $locations, $genres) {
  if (count($locations) == 0 || strcmp("all", $locations[0]) == 0) { // no location, just get artists
      return addAllArtistEvents($artists);
  }

  if (count($genres) == 0) { // location but no genres
    $allEvents = [];

    $artistEvents = addAllArtistEvents($artists);

    foreach ($artistEvents as $key => $artist) {
      $eventLocation = $artist->venue->location;
      $locationString = serialize($eventLocation);
      foreach ($locations as $key => $location) {
        if (strpos($locationString, $location) !== FALSE) {
          if (!in_array($artist, $allEvents))
          array_push($allEvents, $artist);
        }
      }
    }

    return $allEvents;
  }

  // genre and location, don't use artists

  $locationEvents = addAllLocationEvents($locations, $genres);
  return $locationEvents;
}

function addAllArtistEvents($artists) {
  $allEvents = [];

  foreach ($artists as $key => $name) {
    $artistName =  $name->name;
    $events = artistEvents($artistName);
    foreach ($events as $key => $value) {
      array_push($allEvents, $value);
    }
    //$allEvents = array_merge($allEvents, $events);
  }
  //echo "Count: " . count($allEvents);
  return $allEvents;
}

function addAllLocationEvents($locations, $genres) {
  $allEvents = [];
  if (count($genres) > 0) {
    foreach ($locations as $key => $location) {
      foreach ($genres as $key => $genre) {
        $apiUrl = "http://ws.audioscrobbler.com/2.0/?method=geo.getevents&location=" . $location . "&tag=" . $genre . "&api_key=685d1b03f751e03d38e3270b195570c1&format=json&limit=420";
        $events = json_decode(callAPI($apiUrl));
        $events = isset($events->events->event) ? $events->events->event : [];
        $allEvents = array_merge($allEvents, $events);
      }
    }
  } else {
    foreach ($locations as $key => $location) {
      $apiUrl = "http://ws.audioscrobbler.com/2.0/?method=geo.getevents&location=" . $location . "&api_key=685d1b03f751e03d38e3270b195570c1&format=json&limit=420";
      $events = json_decode(callAPI($apiUrl))->events->event;
      $allEvents = array_merge($allEvents, $events);
    }
  }
  return $allEvents;
}

function genres($keywords) {
  $goodGenres = [];
  $genres = callAPI("http://developer.echonest.com/api/v4/genre/list?api_key=PWRWQXN5RAGYJHGGA&format=json&results=1300");
  $genres = json_decode($genres);
  $genres = $genres->response->genres;
  foreach ($keywords as $key => $value) {
    for ($i=0; $i < count($genres); $i++) { 
      $genreName = $genres[$i]->name;
      if (strcmp($genreName, $value) == 0) {
        array_push($goodGenres, $genreName);
      }
    }
  }

  return $goodGenres;
}

function artists($keywords) {
  $keywordString = implode("%20", $keywords);

  $goodArtists = [];
  $artists = callAPI("http://developer.echonest.com/api/v4/artist/extract?api_key=PWRWQXN5RAGYJHGGA&format=json&text=" . $keywordString . "&results=15");
  $artists = json_decode($artists);
  $artists = $artists->response->artists;
  return $artists;
}

function artistEvents($artist) {
  $events = [];
  $events = callAPI("http://ws.audioscrobbler.com/2.0/?method=artist.getevents&artist=" . $artist . "&api_key=685d1b03f751e03d38e3270b195570c1&format=json");
  
  $events = json_decode($events);
  return isset($events->events->event) ? $events->events->event : [];
}

function mapName($n) {
  return($n->name);
}

function mapID($n) {
  return $n->id;
}

?>