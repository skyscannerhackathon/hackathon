<?php
class Sky{

    public function create_session($originplace,$destinationplace,$outbounddate,$inbounddate,$adults,$children,$infants){
        $url = 'http://partners.api.skyscanner.net/apiservices/pricing/v1.0';
		$country = 'GB';
		$currency = 'EUR';
        $locale = 'en-GB';
        $locationschema = 'iata';
        $params = array(
            'apikey'=>'ilw23772659141790428290476649156', // apikey here
            'country'=>$country,
			'currency'=>$currency,
            'locale'=>$locale,
            'originplace'=>strtolower($originplace),
            'destinationplace'=>strtolower($destinationplace) . " -Latlong",
            'outbounddate'=>$outbounddate,
            'inbounddate'=>$inbounddate,
            'locationschema'=>$locationschema,
            'adults'=>$adults,
            'children'=>$children,
            'infants'=>$infants,
            'cabinclass'=>'business'
        );
		$headers = array("Accept: application/json","Content-Type: application/x-www-form-urlencoded");
        $result = $this->makePost($url,$params,'post',$headers);
        //$res = json_decode($result,true);
        echo $result;
        return $result;
    }
	
	private function makePost($url, $params = array(), $method = 'post', $header) {

        $con = curl_init(); //Initialize a cURL session
        if ($method == 'post') {
            $post = '';
			$post_params = array();
			
			foreach ($params as $key => $param) {
				$post_params[] = htmlspecialchars("$key=$param");
			}
			
            $post = implode('&', $post_params);
			
            curl_setopt($con, CURLOPT_URL, $url); //curl_setopt set an option for the cURL transfer
            curl_setopt($con, CURLOPT_POST, true);
			curl_setopt($con, CURLOPT_POSTFIELDS, $post);
			
			//curl_setopt($con,CURLOPT_POSTFIELDS,"apikey=reisepreise524026498357808952413&country=DE&currency=EUR&locale=de-DE&locationschema=iata&originplace=MAD&destinationplace=BCN&outbounddate=2014-04-02&inbounddate=2014-04-15&adults=1&children=0&Infants=0&cabinclass=business");
			
			
        } else { //if not POST
            curl_setopt($con, CURLOPT_URL, $url);
        }
        //$header[] = "Content-Length: ".strlen($post);
		//$header[] = "Content-Length: 199";
        curl_setopt($con, CURLOPT_RETURNTRANSFER, true); // return the transfer as a string
		//curl_setopt($con, CURLOPT_COOKIE, 'PHPSESSID=' . $_COOKIE['PHPSESSID']);
        curl_setopt($con, CURLOPT_CONNECTTIMEOUT,3);
        curl_setopt($con, CURLOPT_TIMEOUT, 30);
        curl_setopt($con, CURLOPT_HTTPHEADER,$header);
        curl_setopt($con, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($con, CURLOPT_VERBOSE, 1);
        curl_setopt($con, CURLOPT_HEADER, 1);

        
		
        $response = curl_exec($con); //Perform a cURL session

        $header_size = curl_getinfo($con, CURLINFO_HEADER_SIZE);
        $header1 = substr($response, 0, $header_size);
        $body1 = substr($response, $header_size);

		list($header1, $body1) = explode("\r\n\r\n", $response, 2);
        $error = curl_error($con); //Return a string containing the last error for the current session
		$responseCode = curl_getinfo($con, CURLINFO_HTTP_CODE);
		$effectiveUrl = curl_getinfo($con, CURLINFO_EFFECTIVE_URL);
		$headers = curl_getinfo($con);

		//echo "ERROR: $error($errno)\n";
		//echo "<textarea>";
		//echo "Response code: ".$responseCode;
        //echo $response;
        $session="";
        $headers = explode("\n", $response);
        foreach($headers as $header) {
            if (stripos($header, 'Location:') !== false) {
                $session = str_replace("Location: ","",trim($header));
            }
        }
        //print_r($session);
		//echo "Response: ".$response;
		//echo "CURLINFO_EFFECTIVE_URL: ".$effectiveUrl."\n\n";
		//echo "URL: $url <br>Method:$method <br> Params<br><pre>";
		//print_r($params);
		//echo "</pre><br>Headers<pre>";
        //echo "</textarea>";
		curl_close($con);
        return $session;
    }



    public function get_live_pricing($sessionLink,$apikey = "ilw23772659141790428290476649156") {
        $url = trim($sessionLink) . "?apiKey=" . $apikey;      
        $data = file_get_contents($url);
//        print_r($data);
        return $data;
    }

    public function get_cheapest_flight_price($origin_place="EDI",$destination_place,$in,$apikey = "ilw23772659141790428290476649156",$out){
        $url = "http://partners.api.skyscanner.net/apiservices/browsegrid/v1.0/". "GB/GBP/en-GB/" . $origin_place ."/" . $destination_place . "-Latlong/" . $out;

        if(empty($in) == true){
            $url = $url . "/" . $in;
        }
        $url = $url . "?apiKey=" . $apikey;
        echo $url;
        $context = stream_context_create(array('http' => array('header' => 'Accept: application/json')));
        $json = json_decode(file_get_contents($url, false, $context));
//        print_r($json->Dates[1][0]->MinPrice);

    }




}



//         $destinationplace='52.237362,21.04753 -Latlong';
//         $destinationplace2='EDI';
//         $outbounddate='2014-12-02';
//         $inbounddate='2014-12-14';
//         $adults=1;
//         $children=0;
//         $infants=0;
// $t = new Sky;

// $a = $t->create_session($originplace,$destinationplace,$outbounddate,$inbounddate,$adults,$children,$infants);
// $b = $t->create_session($originplace,$destinationplace2,$outbounddate,$inbounddate,$adults,$children,$infants);
// //$c = $t ->makeGet($b);
// //print_r($a);
// //print_r($b);
// //print_r($c);
// $t->get_cheapest_flight_price($originplace,$destinationplace,$inbounddate,$outbounddate);
?>